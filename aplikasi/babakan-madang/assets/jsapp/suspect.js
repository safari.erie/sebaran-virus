$(document).ready(function() {

    map = L.map('map').setView([-6.5517758, 106.6291304], 10);
    var tile = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">' +
            'OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © ' +
            '<a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
    }).addTo(map);

    map.on('click', function(e) {
        $('#lat_c').val(e.latlng.lat);
        $('#lon_c').val(e.latlng.lng);

    });
});

function add_suspect() {

    var kode_pasien = $('#kode_pasien').val();
    var jenis_kelamin = $('#jenis-kelamin').val();
    var status_pasien = $('#status-pasien').val();
    var kecamatan = $('#kecamatan').val();
    var kelurahan = $('#kelurahan').val();
    var pengawasan = $('#kelurahan').val();
    var lat = $('#lat_c').val();
    var lon = $('#lon_c').val();
    var url = base_url + 'suspect/add';
    $.ajax({
        url: url,
        method: 'POST',
        data: {
            kode_pasien: kode_pasien,
            jenis_kelamin: jenis_kelamin,
            status_pasien: status_pasien,
            kecamatan: kecamatan,
            kelurahan: kelurahan,
            pengawasan: pengawasan,
            lat: lat,
            lon: lon
        },
        dataType: "json",
        succes: function(r) {
            console.log(r);
        }
    });
}