<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Sebaran Babakan Madang</title>
    <script src="<?php echo base_url();?>assets/jquery.min.js"></script>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fontawesome/css/all.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/datatables.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.css" />
   
    <link href="<?php echo base_url();?>assets/leaflet/leaflet.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/leaflet/leaflet.fullscreen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/leaflet/L.Control.Zoomslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/leaflet/L.Control.Basemaps.css" rel="stylesheet" />


    <link rel="stylesheet" href="<?php echo base_url();?>assets/datepicker/datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/components.css">
    <style>
      #map {
          margin: 0px;
          width: 100%;
          height: 350px;
          padding: 0px;
      }
    </style>
  </head>
  <body class="layout-3">
    
  <div class="container">
