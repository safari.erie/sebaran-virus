<section class="section">

<div class="row pt-3">
    <div class="col-4">
        <div class="card bg-danger text-white">
            <div class="card-header">
                <strong> POSITIF COVID-19 </strong>
            </div>
            <div class="card-body">
                0
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card bg-success text-white">
            <div class="card-header">
                <strong> Sembuh </strong>
            </div>
            <div class="card-body">
                0
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card bg-secondary text-white">
            <div class="card-header">
                <strong> Meninggal </strong>
            </div>
            <div class="card-body">
                0
            </div>
        </div>
    </div>
</div>

<div class="row pt-2">
    <div class="col-6">
        <div class="card bg-light text-dark">
            <div class="card-header">
                <strong> ODP (Orang Dalam Pemantuan) </strong>
            </div>
            <div class="card-body">
                0
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="card bg-light text-dark">
            <div class="card-header">
                <strong> PDP (Pasien Dalam Pengawasan) </strong>
            </div>
            <div class="card-body">
                0
            </div>
        </div>
    </div>
</div>

<div class="row pt-4">
    <div class="col-12">
        <div id="map">
            
        </div>
    </div>
</div>

</section>