<section class="section pt-3">
<form class="form-horizontal" id="form" role="form" method="POST" enctype="multipart/form-data">
    <input type="text" name="lat_c" id="lat_c" class="form-control"/>
    <input type="text" name="lon_c" id="lon_c" class="form-control"/>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label class="col-md-4 ">Kode Pasien</label>
                <div class="col-md-8">
                <input type="text" name="kode_pasien" id="kode_pasien" class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 ">Jenis Kelamin</label>
                <div class="col-md-8">
                    <select class="form-control" name="jenis-kelamin"
                            id="jenis-kelamin">
                            <option value="1">Laki - laki </option>
                            <option value="2">Perempuan </option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 ">Status Pasien</label>
                <div class="col-md-8">
                    <select class="form-control" name="status-pasien"
                            id="status-pasien">
                            <option value="1">Suspect (+) </option>
                            <option value="2">Sembuh </option>
                            <option value="3">Meninggal </option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-6">
                <div class="form-group">
                    <label class="col-md-4">Kecamatan</label>
                    <div class="col-md-8">
                    <select class="form-control" name="kecamatan"
                                id="kecamatan">
                                    
                                <option> </option>
                                    <option value='1'> Babakan Madang </option>
                                </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Kelurahan</label>
                    <div class="col-md-8">
                        <select class="form-control" name="kelurahan"
                                id="kelurahan">
                                    <option value="1"> Babalan Madang </option>
                                    <option value="2"> Bojong Koneng </option>
                                </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4">Pasien Pengawasan</label>
                    <div class="col-md-8">
                        <select class="form-control" name="pengawasan"
                                id="pengawasan">
                                <option value="odp"> ODP</option>
                                <option value="pdp"> PDP</option>
                                </select>
                    </div>
                </div>
        </div>
    </div>

<div class="row pt-4">
    <div class="col-12">
        <div id="map">
            
        </div>
    </div>
</div>


<div class="pd-t-3">
<button type="button" class="btn btn-default waves-effect" onclick="cance;()">Cancel</button>
<button type="button" id="btn_add_suspect" class="btn btn-info waves-effect waves-light" onclick="add_suspect()">Save</button>
</div>
<form>

</section>