
    
  </div>
    <script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.min.js"></script>

    <script src="<?php echo base_url()?>assets/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url()?>assets/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url()?>assets/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>

    <script src="<?php echo base_url()?>assets/leaflet/leaflet.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/leaflet/L.Control.Basemaps.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/leaflet/Leaflet.fullscreen.min.js"></script>
    <script src="<?php echo base_url()?>assets/leaflet/L.Control.Zoomslider.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/assets/datepicker/datepicker.js"></script>
    <!-- jsapp -->
    <?php if (isset($jsapp)): foreach ($jsapp as $js): ?>
            <script type="text/javascript" src="<?php echo base_url() ?>assets/jsapp/<?php echo $js ?>.js"></script>
            <?php
        endforeach;
    endif;
    ?>

<script type="text/javascript">
            var base_url = '<?php echo base_url() ?>';

            //uri string ada di my_controller
            var CONTROLLER = '<?php echo ($this->uri->segment(1) !== FALSE) ? $this->uri->segment(1) : ""; ?>';

            // console.log('tes >> '+CONTROLLER);
        </script>


  </body>
</html>