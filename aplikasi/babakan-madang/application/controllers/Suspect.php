<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suspect extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->model('Suspect_model');
	}
	public function index()
	{
		$data['jsapp']	=  array('suspect');
        $this->load->view('header');
        $this->load->view('suspect');
        $this->load->view('footer',$data);
	}


	function add(){
		


		$data = $this->Suspect_model->add();
		echo json_encode($data);
	}
}
