<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}
	public function index()
	{
		$data['jsapp']	=  array('dashboard');
		$this->load->view('header');
        $this->load->view('main');
        $this->load->view('footer',$data);
	}


	function getSuspect(){

		$data = $this->Main_model->getSuspect();
		echo json_encode($data);
	}
}
