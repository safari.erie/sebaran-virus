<?php
class Suspect_model extends CI_Model 
{

    function __construct()
	{
		parent:: __construct();
    }

    function add(){
        $kode_pasien = $this->input->post('kode_pasien');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$status_pasien = $this->input->post('status_pasien');
		$kecamatan = $this->input->post('kecamatan');
		$kelurahan = $this->input->post('kelurahan');
		$pengawasan = $this->input->post('pengawasan');
		$lat = $this->input->post('lat');
        $lon = $this->input->post('lon');
        
        $sql = "
        INSERT INTO tx_pasien_suspect(kode_pasien,tr_illness_statusid,tr_kecamatanid,tr_sexid,tr_kelurahanid,created_dt,id_pengawasan,geom)
        VALUES('".$kode_pasien."',$status_pasien,$kecamatan,$jenis_kelamin,$kelurahan,now(),$pengawasan, ST_GeomFromText('POINT($lon $lat)', 4326))
        
        ";

        $data = $this->db->query($sql);
        return $data;
    }



}

?>