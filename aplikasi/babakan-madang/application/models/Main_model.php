<?php
class Main_model extends CI_Model 
{

    function __construct()
	{
		parent:: __construct();
    }

    
    function getSuspect(){

        $sql = "
        SELECT
                row_to_json (fc) GJson
                    FROM
                        (
                            SELECT
                                'FeatureCollection' AS TYPE,
                                array_to_json (ARRAY_AGG(f)) AS features
                            FROM
                                (
                                    SELECT
                                        'Feature' AS TYPE,
                                        CAST (
                                            ST_AsGeoJSON (ps.geom) AS json
                                        ) AS geometry,
                                        row_to_json (
                                            (
                                                SELECT
                                                    l
                                                FROM
                                                    (
                                                        SELECT
                                                            (
                                                                CASE
                                                                WHEN ps.tr_illness_statusid = 0 THEN
                                                                    '#ff0000'
                                                                WHEN ps.tr_illness_statusid = 1 THEN
                                                                    '#ffa500'
                                                                WHEN ps.tr_illness_statusid = 2 THEN
                                                                    '#ffff00'
                                                                WHEN ps.tr_illness_statusid = 3 THEN
                                                                    '#008000'
                                                                WHEN ps.tr_illness_statusid = 4 THEN
                                                                    '#0000ff'
                                                                END
                                                            ) AS \"marker_color\",
                                                            'medium' AS \"marker_size\",
                                                            '' AS \"marker_symbol\",
                                                            ps.*
                                                    ) AS l
                                            )
                                        ) AS properties
                                    FROM
                                        PUBLIC .tx_pasien_suspect ps
                                ) AS f
                        ) AS fc
        ";

        $data = $this->db->query($sql)->row();

        return $data;

    }



}

?>